package com.fatimah.restapisederhana.repositories;

import com.fatimah.restapisederhana.models.MCompany;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MCompanyRepository extends JpaRepository<MCompany, String> {
}
