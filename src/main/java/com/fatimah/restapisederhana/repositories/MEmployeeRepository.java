package com.fatimah.restapisederhana.repositories;

import com.fatimah.restapisederhana.models.MEmployee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MEmployeeRepository extends JpaRepository<MEmployee, String> {
}
