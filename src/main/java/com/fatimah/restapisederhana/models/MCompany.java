package com.fatimah.restapisederhana.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity //mendefinisikan class sebagai tabel
@Table(name = "m_company") //penamaan tabel atau custom penamaan
@Data //generate setter, getter, toString, equal, canEqual dan hashcode
@AllArgsConstructor //generate costructor
@NoArgsConstructor //generate costructor kosongan
@SuperBuilder //anotasi yang bisa mengconsume eternal super class agar bisa kita gunakan field2 yang ada di superclass
public class MCompany {
    @Id
    @GeneratedValue(generator = "company-system")
    @GenericGenerator(name = "company-system", strategy = "uuid2")
    @Column(name = "id", nullable = false, length = 64)
    private String id;

    @Column(name = "name", nullable = false, length = 64)
    private String name;

    @Column(name = "address", nullable = false, length = 200)
    private String address;

    @Column(name = "email", nullable = false, length = 64)
    private String email;
}
