package com.fatimah.restapisederhana.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;

@Entity
@Table(name = "m_employee")
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class MEmployee {
    @Id
    @GeneratedValue(generator = "employee-system")
    @GenericGenerator(name = "employee-system", strategy = "uuid2")
    @Column(name = "id", nullable = false, length = 64)
    private String id;

    @ManyToOne
    @JoinColumn(name = "company_id", referencedColumnName = "id",nullable = false)
    private MCompany company;

    @Column(name = "name", nullable = false, length = 64)
    private String name;

    @Column(name = "address", nullable = false, length = 200)
    private String address;

    @Column(name = "phone", nullable = false, length = 20)
    private String phone;
}
