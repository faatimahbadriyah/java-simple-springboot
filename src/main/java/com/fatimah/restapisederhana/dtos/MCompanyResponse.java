package com.fatimah.restapisederhana.dtos;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MCompanyResponse {
    private String id;
    private String name;
    private String address;
    private String email;
}
