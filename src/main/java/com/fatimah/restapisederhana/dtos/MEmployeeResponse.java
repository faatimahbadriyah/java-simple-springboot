package com.fatimah.restapisederhana.dtos;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MEmployeeResponse {
    private String id;
    private MCompanyResponse company;
    private String name;
    private String address;
    private String phone;
}
