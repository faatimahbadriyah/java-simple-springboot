package com.fatimah.restapisederhana.services.impl;

import com.fatimah.restapisederhana.dtos.MCompanyRequest;
import com.fatimah.restapisederhana.dtos.MCompanyResponse;
import com.fatimah.restapisederhana.models.MCompany;
import com.fatimah.restapisederhana.repositories.MCompanyRepository;
import com.fatimah.restapisederhana.services.MCompanyService;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@AllArgsConstructor
public class MCompanyServiceImpl implements MCompanyService {
    private MCompanyRepository companyRepository;

    @Transactional
    @Override
    public MCompanyResponse addCompany(MCompanyRequest request) {
        MCompany requestDataFromTableToRequest = MCompany.builder()
                .name(request.getName())
                .address(request.getAddress())
                .email(request.getEmail())
                .build();
        MCompany savedToDatabase = companyRepository.save(requestDataFromTableToRequest);

        return MCompanyResponse.builder()
                .id(savedToDatabase.getId())
                .name(savedToDatabase.getName())
                .email(savedToDatabase.getEmail())
                .build();
    }

    @SneakyThrows
    @Transactional
    @Override
    public MCompanyResponse updateCompany(String id, MCompanyRequest request) {
        Optional<MCompany> findIdExistingCompany = companyRepository.findById(id);
        if (findIdExistingCompany.isEmpty()){
            throw new Exception("ID Company Not Found");
        }

        log.info("id founds, get all data");

        findIdExistingCompany.get().setName(request.getName());
        findIdExistingCompany.get().setAddress(request.getAddress());
        findIdExistingCompany.get().setEmail(request.getEmail());

        MCompany saveExistingCompanyId = companyRepository.save(findIdExistingCompany.get());

        return MCompanyResponse.builder()
                .id(saveExistingCompanyId.getId())
                .name(saveExistingCompanyId.getName())
                .address(saveExistingCompanyId.getAddress())
                .email(saveExistingCompanyId.getEmail())
                .build();
    }

    @Override
    public List<MCompanyResponse> listCompany() {
        List<MCompany> find = companyRepository.findAll();

        List<MCompanyResponse> returnResponseFromCompany = new ArrayList<>();

        find.forEach(search -> {
            MCompanyResponse searchResponse = MCompanyResponse.builder()
                    .id(search.getId())
                    .name(search.getName())
                    .address(search.getAddress())
                    .email(search.getEmail())
                    .build();
            returnResponseFromCompany.add(searchResponse);
        });
        return returnResponseFromCompany;
    }

    @SneakyThrows
    @Transactional
    @Override
    public void deleteCompany(String id) {
        Optional<MCompany> deleteExistingCompany = companyRepository.findById(id);

        if (deleteExistingCompany.isEmpty()){
            throw new Exception("ID company not found");
        }

        log.info("id founds, delete existing company");

        companyRepository.deleteById(id);
    }

}
