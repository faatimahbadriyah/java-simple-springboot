package com.fatimah.restapisederhana.services.impl;

import com.fatimah.restapisederhana.dtos.*;
import com.fatimah.restapisederhana.models.*;
import com.fatimah.restapisederhana.repositories.*;
import com.fatimah.restapisederhana.services.MEmployeeService;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@AllArgsConstructor
public class MEmployeeServiceImpl implements MEmployeeService {
    private MEmployeeRepository employeeRepository;
    private MCompanyRepository companyRepository;

    private MEmployee transformDataFromModelToRequest(MEmployeeRequest request,
                                                      MCompany company){
        return MEmployee.builder()
                .company(company)
                .address(request.getAddress())
                .name(request.getName())
                .phone(request.getPhone())
                .build();
    }

    private MEmployeeResponse transformDataFromResponseToModel(MEmployee employee, MCompanyResponse response){
        return  MEmployeeResponse.builder()
                .id(employee.getId())
                .company(response)
                .address(employee.getAddress())
                .name(employee.getName())
                .phone(employee.getPhone())
                .build();
    }

    private MCompanyResponse transformDataFromCompanyToResponse(MCompany company){
        return  MCompanyResponse.builder()
                .id(company.getId())
                .email(company.getEmail())
                .address(company.getAddress())
                .name(company.getName())
                .build();
    }

    @SneakyThrows
    @Transactional
    @Override
    public MEmployeeResponse addEmployee(MEmployeeRequest request) {

        Optional<MCompany> findExistingCompanyId  = companyRepository.findById(request.getCompanyId());
        log.info("search id existing company");
        if(findExistingCompanyId.isEmpty()){
            throw  new Exception("ID company not founds ");
        }
        log.info("id company founds, get data company");

        MCompany foundCompanyId = findExistingCompanyId.get();

        MEmployee employeeRequest = transformDataFromModelToRequest(request, foundCompanyId);

        MEmployee responseSaved = employeeRepository.save(employeeRequest);

        MCompanyResponse responseDataCompany = transformDataFromCompanyToResponse(foundCompanyId);

        return transformDataFromResponseToModel(responseSaved, responseDataCompany);
    }

    @SneakyThrows
    @Transactional
    @Override
    public MEmployeeResponse updateEmployee(String id, MEmployeeRequest request) {
        Optional<MCompany> findExistingCompanyId  = companyRepository.findById(request.getCompanyId());
        log.info("search id existing company");
        if(findExistingCompanyId.isEmpty()){
            throw  new Exception("ID company not founds ");
        }
        log.info("id company founds, get data company");
        MCompany foundCompanyId = findExistingCompanyId.get();

        Optional<MEmployee> findEmployeeExistingId  = employeeRepository.findById(id);
        log.info("search id existing Employee");

        if(findEmployeeExistingId.isEmpty()){
            throw  new Exception("ID Employee not founds ");
        }

        log.info("id company founds, get data Employee");

        findEmployeeExistingId.get().setAddress(request.getAddress());
        findEmployeeExistingId.get().setCompany(foundCompanyId);
        findEmployeeExistingId.get().setName(request.getName());
        findEmployeeExistingId.get().setPhone(request.getPhone());

        MEmployee savedDataEmployee = employeeRepository.save(findEmployeeExistingId.get());

        MCompanyResponse responseFromCompany = transformDataFromCompanyToResponse(foundCompanyId);

        return transformDataFromResponseToModel(savedDataEmployee, responseFromCompany);

    }

    @SneakyThrows
    @Override
    public List<MEmployeeResponse> listEmployee() {

        List<MEmployee> findAllExistingEmployee = employeeRepository.findAll();

        List<MEmployeeResponse> returnResponseEmployee = new ArrayList<>();

        findAllExistingEmployee.forEach( searching -> {
            MCompanyResponse getResponseCompany = transformDataFromCompanyToResponse(searching.getCompany());
            MEmployeeResponse getResponseEmployee = transformDataFromResponseToModel(searching, getResponseCompany);

            returnResponseEmployee.add(getResponseEmployee);
        });

        return returnResponseEmployee;
    }

    @SneakyThrows
    @Transactional
    @Override
    public void deleteEmployee(String id) {
        Optional<MEmployee> findExistingEmployeeId = employeeRepository.findById(id);
        if(findExistingEmployeeId.isEmpty()){
            throw  new Exception("ID Not Founds");
        }
        log.info("id founds, execute to delete id");
        employeeRepository.deleteById(id);
    }
}
