package com.fatimah.restapisederhana.services;

import com.fatimah.restapisederhana.dtos.MCompanyRequest;
import com.fatimah.restapisederhana.dtos.MCompanyResponse;

import java.util.List;

public interface MCompanyService {
    MCompanyResponse addCompany(MCompanyRequest request);
    MCompanyResponse updateCompany(String id, MCompanyRequest request);
    List<MCompanyResponse> listCompany();
    void deleteCompany(String id);
}
