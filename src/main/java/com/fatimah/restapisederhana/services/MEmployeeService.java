package com.fatimah.restapisederhana.services;

import com.fatimah.restapisederhana.dtos.MEmployeeRequest;
import com.fatimah.restapisederhana.dtos.MEmployeeResponse;

import java.util.List;

public interface MEmployeeService {
    MEmployeeResponse addEmployee(MEmployeeRequest request);
    MEmployeeResponse updateEmployee(String id, MEmployeeRequest request);
    List<MEmployeeResponse> listEmployee();
    void deleteEmployee(String id);
}
