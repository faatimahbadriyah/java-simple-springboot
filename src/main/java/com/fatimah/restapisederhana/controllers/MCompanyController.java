package com.fatimah.restapisederhana.controllers;

import com.fatimah.restapisederhana.dtos.MCompanyRequest;
import com.fatimah.restapisederhana.dtos.MCompanyResponse;
import com.fatimah.restapisederhana.services.MCompanyService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/company")
@AllArgsConstructor
public class MCompanyController {
    private MCompanyService companyService;

    @PostMapping
    public MCompanyResponse addCompany(@RequestBody @Validated MCompanyRequest request){
        return companyService.addCompany(request);
    }

    @PutMapping("{id}")
    public MCompanyResponse updateCompany(
            @PathVariable("id") String id,
            @RequestBody
            @Validated
                    MCompanyRequest request
    ){
        return companyService.updateCompany(id, request);
    }

    @GetMapping
    public List<MCompanyResponse> listCompany(){
        return companyService.listCompany();
    }

    @DeleteMapping("{id}")
    public String deleteCompany(@PathVariable("id") String id){
        companyService.deleteCompany(id);
        return "Delete successfully with id = " + id;
    }
}
