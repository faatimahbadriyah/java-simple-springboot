package com.fatimah.restapisederhana.controllers;

import com.fatimah.restapisederhana.dtos.MEmployeeRequest;
import com.fatimah.restapisederhana.dtos.MEmployeeResponse;
import com.fatimah.restapisederhana.services.MEmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController //data yang diberikan atau direturn beruoa JSON
@RequestMapping(value = "/employee") //anotasi untuk memberikan url endpoint ke frontend
@AllArgsConstructor //generate method dari service
public class MEmployeeController {
    private MEmployeeService employeeService;

    @PostMapping
    public MEmployeeResponse addEmployee(
            @RequestBody //menampilkan data yagn sukses ditambahkan ke dalam body
            @Validated //validasi inputan sesuai dengan yang di entity
            MEmployeeRequest request
    ){
        return employeeService.addEmployee(request);
    }

    @PutMapping("{id}")
    public MEmployeeResponse updateEmployee(
            @PathVariable("id") String id, //cari dimana lokasi variabel id, jika ditemukan maka edit datanya
            @RequestBody
            @Validated
            MEmployeeRequest request
    ){
        return employeeService.updateEmployee(id, request);
    }

    @GetMapping
    public List<MEmployeeResponse> listEmployee(){
        return employeeService.listEmployee();
    }

    @DeleteMapping("{id}")
    public String deleteEmployee(@PathVariable("id") String id){
        employeeService.deleteEmployee(id);
        return "Delete successfully with id " + id;
    }
}
