FROM openjdk:17-jdk-alpine
ADD target/rest-api-sederhana.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]